import { FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { Component, OnDestroy, OnInit ,ViewChild} from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { Observable } from "rxjs/Rx";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers:[AuthService],
})
export class LoginComponent implements OnInit,OnDestroy {



  loginForm:FormGroup;
  hide:true;
  color:string;
  signupForm:FormGroup
  showSpinner=false;



  constructor(
    private authSvc:AuthService , private router:Router,private fb:FormBuilder) {
      this.loginForm=fb.group({
        'email':[null,Validators.required],
        'password':[null,Validators.required]

      });
    }

  public starredValue;
  @ViewChild("passwordText") input;

  ngOnInit(): void {
    document.body.classList.add('pedro');
  }


  ngOnDestroy():void{
    document.body.classList.remove('pedro');
  }



  async onLogin(){
    const {email,password} = this.loginForm.value;
    try {
      const user = await  this.authSvc.login(email,password);
      if (user) {
        this.router.navigate(['home']);
      }
    } catch (error) {
      console.log(error);
    }
  }


  loadData(){
    this.showSpinner = true ;
    setTimeout(() => {
      this.router.navigate(['home']);
    }, 900 );
  }

}
