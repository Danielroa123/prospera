import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { NavbarComponent } from './navbar/navbar.component';
import {MatIconModule} from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {FormsModule} from '@angular/forms';
import { MenucardComponent } from './menucard/menucard.component'

@NgModule({
  declarations: [HomeComponent,NavbarComponent, MenucardComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatListModule,
    MatIconModule,
    MatToolbarModule,
    MatSidenavModule,
    FormsModule
    ],
  exports: [
    MatListModule,MatIconModule,MatToolbarModule,MatSidenavModule],
})
export class HomeModule {

}
