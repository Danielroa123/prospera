import { Component, OnInit } from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner'; 
import {Event , Router, NavigationStart, NavigationEnd, RouterEvent} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  showLoadingIndicator = true;
  constructor(private spinnerService:NgxSpinnerService,private _router:Router){
    this._router.events.subscribe((routerEvent:Event)=>{
      if (routerEvent instanceof NavigationStart) {
        this.showLoadingIndicator=true;
      } 
      if (routerEvent instanceof NavigationEnd){
        this.showLoadingIndicator=false;
      }
    }
    );
  }
  title = 'Prospera';
  
  spinner(): void{
    this.spinnerService.show();
    setTimeout(() => {
    this.spinnerService.hide();
  },5000);
}
}