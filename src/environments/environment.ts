// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyDhGXh5cnFaR3NGtglgal5RYGU2A3Dalnk",
    authDomain: "login-ng-f0dd5.firebaseapp.com",
    projectId: "login-ng-f0dd5",
    storageBucket: "login-ng-f0dd5.appspot.com",
    messagingSenderId: "563084854911",
    appId: "1:563084854911:web:c2c015dfcd33739447e14a",
    measurementId: "G-XJS4BTVMMB"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
